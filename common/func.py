#!/usr/bin/env python
# -*- coding:utf-8 -*-

import dateutil
import json
import random
import re
import redis
import string
import time

from functools import partial
from datetime import datetime, timedelta


def datetime2timestamp(dt):
    #   datetime2timestamp("2016-1-1 12:12:12") -> 1471234567
    struct_time = time.strptime(dt, "%Y-%m-%d %H:%M:%S")
    return int(time.mktime(struct_time))


def timestamp2datetime(value):
    #   datetime2timestamp(1471234567) -> "2016-1-1 12:12:12"
    struct_time = time.localtime(value)
    dt = time.strftime('%Y-%m-%d %H:%M:%S', struct_time)
    return dt


def utc_datetime2datetime(utc):
    #  utc_datetime2datetime("2016-1-1T12:12:12.123Z") -> "2016-1-1 12:12:12"
    timestamp = int(time.mktime(time.strptime(utc, "%Y-%m-%dT%H:%M:%S.%fZ"))) + 28800
    dt = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(timestamp))
    return dt


def format_time(time_before, format_before, format_after="%Y-%m-%d %H:%M:%S"):
    """
    将非标准格式的时间转换为标准格式的时间
    Author: 胡俊杰
    :param time_before: 非标准格式的时间
    :param format_before: 时间对应的格式
    :param format_after: 标准格式
    :return:
    """
    # format_time("20160101125030", "%Y%m%d%H%M%S") -> '2016-01-01 12:50:30'
    time_struct = time.strptime(time_before, format_before)
    time_after = time.strftime(format_after, time_struct)
    return time_after


def gen_chars(size, charset=string.lowercase+string.digits, prefix="", postfix=""):
    """
    从charset中产生长度为size的字符串
    gen_chars(32, prefix="sessionid.", postfix="bit")
    Author：胡俊杰
    :param size: 字符串长度
    :param charset: 字符集参数
    :param prefix: 前缀
    :param postfix: 后缀
    :return:
    """
    return prefix + "".join((random.choice(charset) for _ in xrange(size))) + postfix


def gen_sep_chars(alist, sep="-", charset=string.lowercase+string.digits, prefix="", postfix=""):
    """
    从alist中获取各部分长度并产生字符串，并以sep为分隔符，将它们连接起来
    gen_sep_chars([4, 8, 2], "-")
    Author：胡俊杰
    :param alist: 长度列表
    :param sep: 分隔符
    :param charset: 字符集
    :param prefix: 前缀
    :param postfix: 后缀
    :return:
    """
    return prefix + sep.join((gen_chars(size, charset) for size in alist)) + postfix


_pool = redis.ConnectionPool(host='localhost', port=6379, db=0)
_redis = redis.StrictRedis(connection_pool=_pool)


def redis_query(ptype, platform, key):
    name = ptype + "_" + platform
    data = _redis.hget(name, key)
    if data:
        return json.loads(data)


def rand_token(platform, uid="20008"):
    token_key = "{uid}_token_{platform}".format(uid=uid, platform=platform)
    val_str = _redis.srandmember(token_key)
    return json.loads(val_str)

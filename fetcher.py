#!/usr/bin/env python
# -*- coding:utf-8 -*-

import logging
import traceback

from Queue import Queue, Empty
from threading import Thread

__all__ = ["fetcher"]


class Downloader(Thread):
    def __init__(self, caller, task_queue, token_list):
        Thread.__init__(self)
        self.daemon = True
        self.caller = caller
        self.task_queue = task_queue
        self.token_list = token_list

    def run(self):
        while 1:
            try:
                account_info = self.task_queue.get_nowait()
                atoken = self.caller(*account_info)
                if isinstance(atoken, list):
                    self.token_list.extend(atoken)
                elif atoken:
                    self.token_list.append(atoken)
            except Empty:
                break
            except:
                logging.error(traceback.format_exc())


def fetcher(caller, account_list, thread_num):
    task_queue = Queue()
    for account in account_list:
        task_queue.put(account)

    threads = []
    token_list = []

    for _ in xrange(thread_num):
        t = Downloader(caller, task_queue, token_list)
        threads.append(t)

    for t in threads:
        t.start()

    for t in threads:
        t.join()
    return token_list

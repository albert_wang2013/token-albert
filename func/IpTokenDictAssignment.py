# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/8/7 12:24
@Author  : Albert
@Func    : 
@File    : IpTokenDictAssignment.py
@Software: PyCharm
"""
import datetime
import json
import logging
import time

import redis
import requests


class IpTokenDictAssignment():
    def __init__(self):
        token_pool = redis.ConnectionPool(
            host="120.77.58.54",
            port=5380,
            db=2,
            password="7ef615c6c32f427d713144f67e2ef14d248c3ccd4bfd6ddf33a4b56878bbcb2f8c72ae185427d853e08de0aa4a8aa7b3a1"
        )
        self.fieldType = None
        self.client = redis.Redis(connection_pool=token_pool)
        self.pipe = self.client.pipeline()
        self.field_fb = 'FaceBook_Token_Account_20191210'    # FaceBook_Token_Account_20191210   FaceBook_Token_Account
        self.field_ins = 'INS_TOKEN_NEW'
        self.field_tube = 'Youtube_Token'

    def get_ip(self):
        url = 'http://39.108.195.197:8888/get_ip'
        response = requests.get(url)
        json_res = json.loads(response.content)
        # print(json.dumps(json_res))
        return json_res

    def deal_ip(self, json_res):
        ip_list = []
        for i in json_res:
            if i['full_name'] == '腾讯云香港':
                # print(i['public_ip'])
                ip_list.append(i['public_ip'])
            if i['full_name'] == '亚马逊云':
                # print(i['public_ip'])
                ip_list.append(i['public_ip'])
        print('len_ip_list:', len(ip_list))
        return ip_list

    def get_token(self):
        itemDict_fb = self.client.hgetall(self.field_fb)
        token_list_fb = []
        for key, value in itemDict_fb.items():
            token_list_fb.append(value.decode())
        print('len_token_list_fb:', len(token_list_fb))
        token_list_ins = [i.decode() for i in self.client.smembers(self.field_ins)]
        print('len_token_list_ins:', len(token_list_ins))
        token_list_tube = [i.decode() for i in self.client.smembers(self.field_tube)]
        print('len_token_list_tube:', len(token_list_tube))
        return token_list_fb, token_list_ins, token_list_tube

    def deal_ip_token_list(self, ip_list, token_list_fb, token_list_ins, token_list_tube):
        """fb"""
        token_list = token_list_fb + [None] * \
            (len(ip_list) - len(token_list_fb) % len(ip_list))

        def f_lambda(token_list, n): return [[row[ix] for row in [
            token_list[i:i + n] for i in range(0, len(token_list), n)] if row[ix]] for ix in range(n)]
        result_fb = dict(zip(ip_list, f_lambda(token_list, len(ip_list))))
        # print(json.dumps(result_fb))
        """ins"""
        token_list = token_list_ins + [None] * \
                     (len(ip_list) - len(token_list_ins) % len(ip_list))

        def f_lambda(token_list, n): return [[row[ix] for row in [
            token_list[i:i + n] for i in range(0, len(token_list), n)] if row[ix]] for ix in range(n)]

        result_ins = dict(zip(ip_list, f_lambda(token_list, len(ip_list))))
        # print(json.dumps(result_ins))
        """ tube """
        token_list = token_list_tube + [None] * \
                     (len(ip_list) - len(token_list_tube) % len(ip_list))

        def f_lambda(token_list, n): return [[row[ix] for row in [
            token_list[i:i + n] for i in range(0, len(token_list), n)] if row[ix]] for ix in range(n)]

        result_tube = dict(zip(ip_list, f_lambda(token_list, len(ip_list))))
        return result_fb, result_ins, result_tube

    def token_write_redis(self, result_fb, result_ins, result_tube):
        self.client.delete('FaceBook_Token_Account_Dict')
        self.client.delete('INS_TOKEN_Dict')
        self.client.delete('Youtube_TOKEN_Dict')
        for key, value in (result_fb.items()):
            self.client.hset('FaceBook_Token_Account_Dict', key, value)
        for key, value in (result_ins.items()):
            self.client.hset('INS_TOKEN_Dict', key, value)
        for key, value in (result_tube.items()):
            self.client.hset('Youtube_TOKEN_Dict', key, value)
        print('token_write_redis success')

    def main(self):
        json_res = self.get_ip()
        ip_list = self.deal_ip(json_res)
        token_list_fb, token_list_ins, token_list_tube = self.get_token()
        result_fb, result_ins, result_tube = self.deal_ip_token_list(ip_list, token_list_fb, token_list_ins, token_list_tube)
        result_fb['{}'.format('121.35.103.79')] = token_list_fb
        result_fb['{}'.format('183.17.228.233')] = token_list_fb
        result_fb['{}'.format('47.90.58.29')] = token_list_fb
        print(json.dumps(result_fb))

        result_ins['{}'.format('121.35.103.79')] = token_list_ins
        result_ins['{}'.format('183.17.228.233')] = token_list_ins
        result_ins['{}'.format('47.90.58.29')] = token_list_ins
        print(json.dumps(result_ins))

        result_tube['{}'.format('121.35.103.79')] = token_list_tube
        result_tube['{}'.format('183.17.228.233')] = token_list_tube
        result_tube['{}'.format('47.90.58.29')] = token_list_tube
        print(json.dumps(result_tube))

        self.token_write_redis(result_fb, result_ins, result_tube)


if __name__ == '__main__':
    IpTokenDictAssignment().main()
    start_ = datetime.datetime.now().replace(microsecond=0)
    print('current time:', start_)
    time.sleep(3600*24*0.99)

# #!/usr/bin/env python
# # -*- coding: utf-8 -*-
#
# import requests
# import redis
# import json
#
#
# def get_cookies():
#     host = '120.77.58.54'
#     port = 5380
#     db = 2
#     password = '7ef615c6c32f427d713144f67e2ef14d248c3ccd4bfd6ddf33a4b56878bbcb2f8c72ae185427d853e08de0aa4a8aa7b3a1'
#     pool = redis.ConnectionPool(host=host, port=port, db=db, password=password)
#     client = redis.Redis(connection_pool=pool)
#     cookie_list = client.hgetall('Instagram_Cookie')
#     return cookie_list
#
#
# def get_response(cookie):
#     url = 'https://www.instagram.com/graphql/query/?query_hash=49699cdb479dd5664863d4b647ada1f7&variables={"shortcode":"Bq_J_GSFXH3","child_comment_count":3,"fetch_comment_count":40,"parent_comment_count":24,"has_threaded_comments":false}'
#     headers = {
#         'authority': 'www.instagram.com',
#         'method': 'GET',
#         'scheme': 'https',
#         'accept': '*/*',
#         'accept-encoding': 'gzip, deflate, br',
#         'accept-language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
#         'cookie': cookie,
#         'dnt': '1',
#         'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36',
#         'x-requested-with': 'XMLHttpRequest'
#     }
#     response = requests.get(url=url, headers=headers)
#     try:
#         print(json.loads(response.content.decode())['status'])
#     except Exception:
#         print(cookie)
#
#
# def main():
#     cookie_list = get_cookies()
#     for (key, cookie) in cookie_list.items():
#         # print(list(cookie_list).index(cookie))
#         get_response(cookie.decode())
#
#
# if __name__ == '__main__':
#     main()
#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import redis
import json



def get_response(cookie):
    url = 'https://www.instagram.com/graphql/query/?query_hash=49699cdb479dd5664863d4b647ada1f7&variables={"shortcode":"Bq_J_GSFXH3","child_comment_count":3,"fetch_comment_count":40,"parent_comment_count":24,"has_threaded_comments":false}'
    headers = {
        'authority': 'www.instagram.com',
        'method': 'GET',
        'scheme': 'https',
        'accept': '*/*',
        'accept-encoding': 'gzip, deflate, br',
        'accept-language': 'zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7',
        'cookie': cookie,
        'dnt': '1',
        'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36',
        'x-requested-with': 'XMLHttpRequest'
    }
    proxies ={
        "http": "http://127.0.0.1:12222",
        "https": "https://127.0.0.1:12222",
              }
    try:
        # 有数据
        response = requests.get(url=url, headers=headers, proxies=proxies, timeout=30)
        print(response.status_code, json.loads(response.content.decode())['status'])
    except Exception:
        # cookies失效的情况
        print(cookie)

        return cookie



def main():
    es_redis = redis.Redis(connection_pool=redis.ConnectionPool(host='120.77.58.54', port=5380, db=2, password='7ef615c6c32f427d713144f67e2ef14d248c3ccd4bfd6ddf33a4b56878bbcb2f8c72ae185427d853e08de0aa4a8aa7b3a1'))
    list_tokens = es_redis.smembers('INS_TOKEN_NEW')
    print(list_tokens)
    list_tokens = [i.decode() for i in list_tokens]
    print(list_tokens)
    print(len(list_tokens))
    # list_tokens = [
    # ]
    for token in list_tokens:
        s = get_response(token)
        if s:
            es_redis.srem('INS_TOKEN_NEW', s.encode())


if __name__ == '__main__':
    main()

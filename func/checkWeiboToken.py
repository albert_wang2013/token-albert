#!/usr/bin/env python
# -*- coding: utf-8 -*-

from requests_toolbelt import MultipartEncoder
from email.mime.text import MIMEText
from email.header import Header
import requests
import smtplib
import hashlib
import asyncio
import aiohttp
import redis
import json
import time


class OperateRedis():
    def __init__(self):
        token_pool = redis.ConnectionPool(
            host="120.77.58.54",
            port=5380,
            db=2,
            password="7ef615c6c32f427d713144f67e2ef14d248c3ccd4bfd6ddf33a4b56878bbcb2f8c72ae185427d853e08de0aa4a8aa7b3a1"
        )
        self.client = redis.Redis(connection_pool=token_pool)
        self.pipe = self.client.pipeline()

    def get_field_type(self, field):
        return self.client.type(field).decode()

    def get_length(self, field):
        fieldType = self.get_field_type(field)
        if fieldType == 'set':
            length = self.client.scard(field)
        elif fieldType == 'hash':
            length = self.client.hlen(field)
        else:
            length = None
        return length

    def get_all(self, field):
        fieldType = self.get_field_type(field)
        if fieldType == 'set':
            length = self.get_length(field)
            itemList = self.client.sscan(field, cursor=0, count=length)
            if itemList:
                itemList = itemList[1]
                for i in range(len(itemList)):
                    itemList[i] = itemList[i].decode().strip('"')
            return itemList
        elif fieldType == 'hash':
            itemDict = self.client.hgetall(field)
            newItemDict = dict()
            for key, value in itemDict.items():
                newItemDict[key.decode()] = value.decode()
            return newItemDict

    def add_error_item(self, field, addListOrDict):
        es_redis = redis.Redis(connection_pool=redis.ConnectionPool(host='120.77.58.54', port=5380, db=2,
                                                                    password='7ef615c6c32f427d713144f67e2ef14d248c3ccd4bfd6ddf33a4b56878bbcb2f8c72ae185427d853e08de0aa4a8aa7b3a1'))
        with es_redis.pipeline(transaction=False) as p:
            for i in addListOrDict:
                p.sadd(field, i)
            p.execute()
        print('addListOrDict ok!')

    def remove_item(self, field, removeList):
        es_redis = redis.Redis(connection_pool=redis.ConnectionPool(host='120.77.58.54', port=5380, db=2,
                                                                    password='7ef615c6c32f427d713144f67e2ef14d248c3ccd4bfd6ddf33a4b56878bbcb2f8c72ae185427d853e08de0aa4a8aa7b3a1'))
        with es_redis.pipeline(transaction=False) as p:
            for i in removeList:
                p.srem(field, i)
            p.execute()
        print('removeList ok!')

    def add_new_item(self, field, addListOrDict):
        es_redis = redis.Redis(connection_pool=redis.ConnectionPool(host='120.77.58.54', port=5380, db=2,
                                                                    password='7ef615c6c32f427d713144f67e2ef14d248c3ccd4bfd6ddf33a4b56878bbcb2f8c72ae185427d853e08de0aa4a8aa7b3a1'))
        with es_redis.pipeline(transaction=False) as p:
            for i in addListOrDict:
                p.sadd(field, i)
            p.execute()
        print('addListOrDict ok!')


async def request_profile(token):
    gsid, s = token.split(',')
    # print(token)
    # gsid, s = '_2A252ljMnDeRxGeBI7VYY9ibFwzWIHXVTAsHvrDV6PUJbkdAKLWnnkWpNRm94eZwhtGkS5qikip8NELMbfQ8GW-wb,3c011111'.split(',')
    url = 'https://api.weibo.cn/2/users/show?from=1089195010&uid=%s&gsid=%s&s=%s&' \
          'c=android&lang=zh_CN&ua=iPhone6,2__weibo__5.3.0__iphone__os8.1' \
          % ('2803301701', gsid, s)
    HEADERS = {
        'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) '
                      'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36',
        'Accept-Encodingutf-8': 'utf-8'
    }
    async with aiohttp.ClientSession() as session:
        async with session.get(url=url, headers=HEADERS) as resp:
            content = await resp.json(content_type=None)
            print(resp.status)
            await asyncio.sleep(1)
            return content


async def check():
    r = OperateRedis()  # 初始化redis
    field = 'WEIBO_TOKEN_NEW'
    errorField = 'WEIBO_TOKEN_ERROR'
    newField = 'WEIBO_TOKEN_NEW_1025'
    while True:
        tokenListLength = r.get_length(field)
        tokenDict = r.get_all(field) if tokenListLength > 0 else None
        # print(tokenListLength, tokenDict)
        errorList = []
        removeList = []
        newList = []
        count = 1
        for token in tokenDict[:50]:
            body = await request_profile(token)
            if body:
                if 'errmsg' in body.keys():
                    print(body)
                    errorMsg = body['errmsg']
                    errorList.append('{0} -- {1}'.format(token, errorMsg))
                else:
                    newList.append(token)
                removeList.append(token)
                count += 1
        r.add_error_item(errorField, errorList)
        r.remove_item(field, removeList)
        r.add_new_item(newField, newList)
        print(removeList, len(removeList))
        print(newList, len(newList))
        print(errorList, len(errorList))
        # time.sleep(60 * 60 * 24)  # 每12小时检查一次


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(check())
    loop.run_forever()


#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests
import redis
import json


def get_tokens():
    host = '120.77.58.54'
    port = 5380
    db = 2
    password = '7ef615c6c32f427d713144f67e2ef14d248c3ccd4bfd6ddf33a4b56878bbcb2f8c72ae185427d853e08de0aa4a8aa7b3a1'
    pool = redis.ConnectionPool(host=host, port=port, db=db, password=password)
    client = redis.Redis(connection_pool=pool)
    token_list = client.smembers('Youtube_Token')
    return token_list, client


def get_response(index, token, client):
    try:
        proxy = {
            "http": "http://127.0.0.1:12222",
            "https": "https://127.0.0.1:12222",
                 }
        url = 'https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails,statistics&id=IePnBrqEuKA&key={0}'.format(token)
        response = requests.get(url=url, proxies=proxy)
        jsonObj = json.loads(response.content.decode())
        # print(json.dumps(jsonObj))
        # print(index)
        if 'error' in jsonObj:
            print(index, token, json.dumps(jsonObj))

            if 'The quota will be reset at midnight Pacific Time' in jsonObj['error']['errors'][0]['message']:
                pass
            elif 'The request cannot be completed because you have exceeded your' in jsonObj['error']['errors'][0]['message']:
                pass
            else:
                # return None, token
                client.srem('Youtube_Token', token)
    except:
        pass




def main():
    token_list, client = get_tokens()
    print(len([bytes.decode(i) for i in token_list]))
    # token_list = []
    for token in token_list:
        index = list(token_list).index(token)
        token = token.decode().replace('"', '')
        get_response(index, token, client)
        # break


if __name__ == '__main__':
    main()

# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/7/24 11:15
@Author  : Albert
@Func    : 
@File    : redis_select_fb.py
@Software: PyCharm
"""
import asyncio
import aiohttp
import redis
import json
import time


class OperateRedis():
    def __init__(self):
        token_pool = redis.ConnectionPool(
            host="120.77.58.54",
            port=5380,
            db=2,
            password="7ef615c6c32f427d713144f67e2ef14d248c3ccd4bfd6ddf33a4b56878bbcb2f8c72ae185427d853e08de0aa4a8aa7b3a1"
        )
        self.client = redis.Redis(connection_pool=token_pool)
        self.pipe = self.client.pipeline()
        self.fieldType = None
        self.field = 'FaceBook_Account_Token'  # FaceBook_Account_Token    FaceBook_Token_Account formal

    def get_field_type(self):
        return self.client.type(self.field).decode()

    def get_length(self, field):
        self.fieldType = self.get_field_type()
        print(self.fieldType)
        if self.fieldType == 'set':
            length = self.client.scard(field)
        elif self.fieldType == 'hash':
            length = self.client.hlen(field)
        else:
            length = self.client.llen(field)
        return length, self.fieldType

    def get_all_token(self):
        itemDict = self.client.hgetall(self.field)
        # print(itemDict)
        newItemDict = dict()
        for key, value in itemDict.items():
            newItemDict[key.decode()] = value.decode()
        return newItemDict

    def get_all_token1(self):
        itemDict = [i.decode() for i in self.client.lrange(self.field, 0, 1000)]
        return itemDict

    async def check(self):
        r = OperateRedis()  # 初始化redis
        tokenListLength, type_ = r.get_length(self.field)
        print('{}'.format(self.field) ,tokenListLength)
        if type_ == 'set' or type_ == 'hash':
            tokens = self.get_all_token()
        else:
            tokens = self.get_all_token1()
        self.write_(tokens)   # token写到本地
        # self.rm_token(tokens, r)
        # self.add_token_redis(r)

    def add_token_redis(self, r):
        tokens = []
        with open('FaceBook_Account_Token_2019_old.txt', 'r') as f:
            for i in f.readlines():
                i = i.replace('\n', '')
                tokens.append(json.loads(i))
        tokens = tokens[:100]
        for token in tokens:
            for key, value in token.items():
                self.client.hset('FaceBook_Token_Account', key, value)

    def rm_token(self, tokens, r):
        token_list = []
        with open('old_token.txt', 'r') as f:  # 原始的token
            for i in f.readlines():
                i = i.replace('\n','')
                token_list.append(i)
        print('old_token', len(token_list))
        remove_list = []
        for key, value in tokens.items():
            data = {
                key: value,
            }
            if value not in token_list:
                remove_list.append(key)
        print('remove_list', len(remove_list))
        r.remove_item(remove_list)


    def remove_item(self, removeList):
        # 从正式里面移除token
        for item in removeList:
            self.pipe.hdel(self.field, item)
        self.pipe.execute()

    def write_(self, tokens):
        with open('{}.txt'.format(self.field), 'a+') as f:
            for key, value in tokens.items():
                data = {
                    key: value,
                }
                f.write(json.dumps(data) + '\n')
                f.flush()

        # with open('{}_账号.txt'.format(self.field), 'a+') as f:
        #     for i in tokens:
        #         f.write(i + '\n')
        #         f.flush()


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.run_until_complete(OperateRedis().check())
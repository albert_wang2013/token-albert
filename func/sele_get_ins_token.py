# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/4/17 11:23
@Author  : Albert
@Func    :
@File    : qzone_cookie.py
@Software: PyCharm
"""
from selenium import webdriver
import time


URL = 'https://www.instagram.com/accounts/login/?source=auth_switcher'


def begin():
    users =[
        'dallasbeard98:MAm917lv489617205',
'nathanortega9156:XPFZLJWsQyo7M7563012',
'wilburpitts34:REBNOIKEqkIi98254',
'arongarza8:LNDWGI2HkZlO15483792',
'leroystephens3491:UOHEABCDiQEKSf7258091',
    ]



    token_list = []
    error_account = []
    with open('ins_token.txt', 'a+') as f:
        for i in users:
            try:
                user, pwd = i.split(':')
                options = webdriver.ChromeOptions()
                # options.add_argument("--no-sandbox")
                # options.add_argument('headless')

                # driver = webdriver.Chrome("/root/albert-test-dir/chromedriver", chrome_options=options)
                driver = webdriver.Chrome()
                driver.get(URL)
                time.sleep(2)
                driver.find_element_by_name('username').send_keys(user)
                driver.find_element_by_name('password').send_keys(pwd)
                driver.find_element_by_xpath('//button[@type="submit"]').click()
                time.sleep(10)
                cookies = driver.get_cookies()
                driver.close()
                print('{} 的cookies是: {}'.format(i, cookies))

                # cookies 解析获取
                token_ = {}
                for elem in cookies:
                    if elem['name'] == 'mid':
                        token_['mid'] = elem['value']
                    if elem['name'] == 'csrftoken':
                        token_['csrftoken'] = elem['value']
                    if elem['name'] == 'ds_user_id':
                        token_['ds_user_id'] = elem['value']
                    if elem['name'] == 'sessionid':
                        token_['sessionid'] = elem['value']
                if token_['mid'] and token_['csrftoken'] and token_['ds_user_id'] and token_['sessionid']:
                    token_ = '"mid={}; mcd=3; csrftoken={}; rur=FRC; ds_user_id={}; sessionid={}",'\
                        .format(token_['mid'], token_['csrftoken'], token_['ds_user_id'], token_['sessionid'])
                    print(token_)
                    f.write(token_ + '\n')
                    f.flush()
                    token_list.append(token_)
                else:
                    print('account is error')
                    continue
            except Exception as e:
                print(e)
                error_account.append(i)
        print(error_account, '\n')


if __name__ == '__main__':
    begin()



# from selenium import webdriver
# import time
#
# if __name__ == '__main__':
#     options = webdriver.ChromeOptions()
#     options.add_argument("--no-sandbox")
#     driver = webdriver.Chrome("/root/albert-test-dir/chromedriver",chrome_options=options)
#     driver.get('https://www.baidu.com')
#     print(driver.title)
#     time.sleep(1)
#     driver.close()
# -*- coding: utf-8 -*-

"""
@Python version:3.6+
@Time    : 2019/7/24 14:45
@Author  : Albert
@Func    : 
@File    : testd_fb.py
@Software: PyCharm
"""
import json

import redis


LOCALPOOL = redis.ConnectionPool(
            host="120.77.58.54",
            port=5380,
            db=11,
            password="7ef615c6c32f427d713144f67e2ef14d248c3ccd4bfd6ddf33a4b56878bbcb2f8c72ae185427d853e08de0aa4a8aa7b3a1"
        )
LOCALPOOL_new = redis.ConnectionPool(
            host="120.77.58.54",
            port=5380,
            db=2,
            password="7ef615c6c32f427d713144f67e2ef14d248c3ccd4bfd6ddf33a4b56878bbcb2f8c72ae185427d853e08de0aa4a8aa7b3a1"
        )
_redis = redis.StrictRedis(connection_pool=LOCALPOOL)
_redis_new = redis.StrictRedis(connection_pool=LOCALPOOL_new)

db_get = 'facebook_token'   # FaceBook_Account_Token   FaceBook_Token_Account_20191210
db_save = 'FaceBook_Token_Account_20191210'     # FaceBook_Token_Account_test

def hkey_get_del():
    # 获取所有的keys，并且随机取出多少key，并删除该key
    all_keys = [i.decode() for i in _redis.hkeys(db_get)]
    print(len(_redis.hkeys(db_get)), all_keys)
    for key in all_keys[:20]:
        value = _redis.hget(db_get, key).decode()
        _redis_new.hset(db_save, key, value)    # 取出token新增到db中
        _redis.hdel(db_get, key)    # 从旧db中删除token

hkey_get_del()


def save_token():
    # 存token到redis中
    tokens = []
    with open('FaceBook_Account_Token_2019.txt', 'r') as f:
        for i in f.readlines():
            i = i.replace('\n', '')
            tokens.append(json.loads(i))
    tokens = tokens
    for token in tokens:
        for key, value in token.items():
            _redis.hset(db_save, key, value)

# save_token()


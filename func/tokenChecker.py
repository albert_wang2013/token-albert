#!/usr/bin/env python
# -*- coding: utf-8 -*-

from requests_toolbelt import MultipartEncoder
from email.mime.text import MIMEText
from email.header import Header
import requests
import smtplib
import hashlib
import asyncio
import aiohttp
import redis
import json
import time

from tornado import escape


class OperateRedis():
    def __init__(self):
        token_pool = redis.ConnectionPool(
            host="120.77.58.54",
            port=5380,
            db=2,
            password="7ef615c6c32f427d713144f67e2ef14d248c3ccd4bfd6ddf33a4b56878bbcb2f8c72ae185427d853e08de0aa4a8aa7b3a1"
        )
        self.client = redis.Redis(connection_pool=token_pool)
        self.pipe = self.client.pipeline()

    def get_field_type(self, field):
        return self.client.type(field).decode()

    def get_length(self, field):
        fieldType = self.get_field_type(field)
        if fieldType == 'set':
            length = self.client.scard(field)
        elif fieldType == 'hash':
            length = self.client.hlen(field)
        else:
            length = None
        return length

    def get_all(self, field):
        fieldType = self.get_field_type(field)
        if fieldType == 'set':
            length = self.get_length(field)
            itemList = self.client.sscan(field, cursor=0, count=length)
            if itemList:
                itemList = itemList[1]
                for i in range(len(itemList)):
                    itemList[i] = itemList[i].decode().strip('"')
            return itemList
        elif fieldType == 'hash':
            itemDict = self.client.hgetall(field)
            newItemDict = dict()
            for key, value in itemDict.items():
                newItemDict[key.decode()] = value.decode()
            return newItemDict

    def add_item(self, field, addListOrDict):
        fieldType = self.get_field_type(field)
        if fieldType == 'set':
            for item in addListOrDict:
                self.pipe.sadd(field, item.strip())
            self.pipe.execute()
        elif fieldType == 'hash':
            for item in addListOrDict.keys():
                self.pipe.hset(field, item, addListOrDict[item])
            self.pipe.execute()

    def remove_item(self, field, removeList):
        fieldType = self.get_field_type(field)
        if fieldType == 'hash':
            for item in removeList:
                self.pipe.hdel(field, item)
            self.pipe.execute()


class SendEmail():
    def __init__(self, errorList, receiverList):
        # 第三方 SMTP 服务
        mail_host = 'smtp.qiye.aliyun.com'  # 设置服务器
        mail_port = 465  # 25 为阿里云企业邮箱的 SMTP SSL端口号
        mail_user = 'token-server@bit-beyond.com'  # 用户名
        mail_pass = '12qwaszxoffice*'  # 口令

        self.smtpObj = smtplib.SMTP_SSL()
        # smtpObj.set_debuglevel(1)  # 设置为 1 开启调试信息
        self.smtpObj.connect(mail_host, mail_port)
        self.smtpObj.login(mail_user, mail_pass)

        self.errorList = errorList
        self.receiverList = receiverList

    def send_mail(self, receiver, validLength):
        sender = 'token-server@bit-beyond.com'

        errorLength = len(self.errorList)
        message = MIMEText('发现新的失效Token {0} 个，剩余 {1} 个有效Token，失效Token分别是: \n{2}'.format(
            errorLength, validLength, '\n'.join(self.errorList)), 'plain', 'utf-8')  # 正文
        message['From'] = Header('BitSpaceman', 'utf-8')  # 显示的发件人
        message['To'] = Header(receiver['name'], 'utf-8')  # 显示的收件人

        subject = '新增失效Token--FaceBook'  # 邮件主题
        message['Subject'] = Header(subject, 'utf-8')

        try:
            self.smtpObj.sendmail(sender, receiver['address'], message.as_string())
        except smtplib.SMTPException as e:
            print('Error: Mail sends failed! The error is: {0}'.format(e))


class SendMessage():
    def __init__(self, errorList, receiverList):
        self.errorLength = len(errorList)
        self.receiverList = receiverList

    def send_msg(self, receiver, validLength):
        phone = receiver['phone']
        if phone and phone != '':
            appId = '27058'
            appKey = 'd202ed022b732add8df2201125fcb945'
            project = '6UxBa2'
            tsUrl = 'https://api.mysubmail.com/service/timestamp'
            response = requests.get(tsUrl)
            timestamp = json.loads(response.content)['timestamp']
            signType = 'md5'
            params = {
                'appid': appId,
                'to': phone,
                'project': project,
                'vars': json.dumps({'count': str(self.errorLength), 'left': str(validLength)}),
                'timestamp': timestamp,
                'sign_type': signType
            }
            paramListSorted = sorted(params.items(), key=lambda item: item[0])
            for i in range(len(paramListSorted)):
                key = paramListSorted[i][0]
                value = paramListSorted[i][1]
                paramListSorted[i] = '{0}={1}'.format(key, value)
            paramStr = '&'.join(paramListSorted)
            signatureStr = '{0}{1}{2}{3}{4}'.format(
                            appId, appKey, paramStr, appId, appKey)
            m = hashlib.md5()
            m.update(signatureStr.encode())
            signature = m.hexdigest()
            params['signature'] = signature

            url = 'https://api.mysubmail.com/message/xsend'
            requests.post(url, data=params)


async def request_relation(token):
    url = 'https://graph.facebook.com/graphql'
    headers = {
        'Authorization': 'OAuth {0}'.format(token.split('####')[0]),
        'X-FB-Connection-Type': 'WIFI',
        'x-fb-net-hni': '46001',
        'x-fb-sim-hni': '46009',
        'x-fb-net-sid': '',
        'X-FB-HTTP-Engine': 'Apache',
        'Content-Encoding': 'gzip',
        'Host': 'graph.facebook.com',
        'Connection': 'Keep-Alive',
        'User-Agent': '[FBAN/FB4A;FBAV/49.0.0.12.152;FBBV/15693254;FBDM/{density=3.0,width=1080,height=1920};FBLC/zh_CN;FBCR/;FBMF/Xiaomi;FBBD/Xiaomi;FBPN/com.facebook.katana;FBDV/Redmi Note 3;FBSV/6.0.1;FBOP/1;FBCA/armeabi-v7a:armeabi;]',
        'Accept-Encoding': 'gzip',
    }
    query = {
        '0': '4',  # Mark Zuckerbuger
        '1': '1080',
        '2': 'image/jpeg',
        '3': '540',
        '4': 'true',
        '5': 'false',
        '6': '3',
        '7': '96',
        '8': 'image/x-auto',
        '10': 'true',
        '12': 'true',
        '13': '288',
        '14': 'false',
        '15': 'true',
        '16': 'true',
        '17': '282',
        '18': 'false',
        '19': '288',
        '21': 'ANDROID_PROFILE_INTRO_CARD_HEADER',
        '22': 'false',
        '23': 1
    }
    body = {
        'query_params': json.dumps(query),
        'method': 'get',
        'query_id': '10154034419546729',  # 此value为获取用户信息的固定方法代码
        'strip_nulls': 'true',
        'strip_defaults': 'true',
        'locale': 'zh_CN',
        'client_country_code': 'CN',
        'fb_api_req_friendly_name': 'UserTimelineQuery',
        'fb_api_caller_class': 'com.facebook.timeline.TimelineFragment'
    }
    body = MultipartEncoder(body)
    data = body.read()
    headers['Content-Type'] = body.content_type
    async with aiohttp.ClientSession() as session:
        async with session.post(url=url, headers=headers, data=data, proxy="http://127.0.0.1:12222") as resp:
            content = await resp.text()
            return content

async def request_profile(token):
    host = "https://graph.facebook.com/"
    uid = '4'
    start_url = host + uid + "?fields=picture,name,username,birthday,gender,location,hometown,education,email," \
                             "work,subscribers,relationship_status,quotes,is_verified,religion" + "&access_token=" + token + "&redirect=false"
    search_headers = {
        'accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        'accept-encoding': "gzip, deflate, sdch, br",
        'accept-language': "zh-CN,zh;q=0.8,en;q=0.6",
        'cache-control': "no-cache",
        'upgrade-insecure-requests': "1",
        'user-agent': "Mozilla/5.0 (X11; Linux i686; U;) Gecko/20070322 Kazehakase/0.4.5",
    }
    async with aiohttp.ClientSession() as session:
        async with session.get(url=start_url, headers=search_headers, proxy="http://127.0.0.1:12222") as resp:
            content = await resp.text(encoding='utf8')
            return content

async def request_profile_get_type(token):
    host = "https://graph.facebook.com/"
    uid = '4'
    query = 'SELECT type FROM profile WHERE id=' + uid
    start_url = host + "fql?access_token=" + \
                token + "&q=" + escape.url_escape(query)
    search_headers = {
        'accept': "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        'accept-encoding': "gzip, deflate, sdch, br",
        'accept-language': "zh-CN,zh;q=0.8,en;q=0.6",
        'cache-control': "no-cache",
        'upgrade-insecure-requests': "1",
        'user-agent': "Mozilla/5.0 (X11; Linux i686; U;) Gecko/20070322 Kazehakase/0.4.5",
    }
    async with aiohttp.ClientSession() as session:
        async with session.get(url=start_url, headers=search_headers) as resp:
            content = await resp.text(encoding='utf8')
            return content

async def check(receiverList):
    r = OperateRedis()  # 初始化redis
    field = 'FaceBook_Token_Account_20191210'
    errorField = 'FaceBook_Token_Disable'
    while True:
        tokenListLength = r.get_length(field)
        tokenDict = r.get_all(field) if tokenListLength > 0 else None
        errorList = []
        removeList = []
        count = 1
        for account, token in tokenDict.items():
            body = json.loads(await request_profile(token))
            # print(body)
            print('{0} / {1}'.format(str(count), account))
            print(body)
            if 'error' in body.keys():
                if 'The user is enrolled in a blocking' in body['error']['message'] or 'because the user changed their password or Facebook' in body['error']['message']\
                       or 'Error validating access token' in body['error']['message']:
                    errorMsg = body['error']['message']
                    errorList.append('{0} -- {1}'.format(account, errorMsg))
                    removeList.append(account)
            count += 1
            # await asyncio.sleep(2)
        r.add_item(errorField, errorList)
        r.remove_item(field, removeList)
        # validLength = r.get_length(field)
        print(len(removeList))
        # if errorList and errorList != []:
        #     email = SendEmail(errorList, receiverList)
        #     # message = SendMessage(errorList, receiverList)
        #     for receiver in receiverList:
        #         email.send_mail(receiver, validLength)  # 发送邮件
        #         # message.send_msg(receiver, validLength)  # 发送短信

        time.sleep(60 * 60 * 24 * 3)  # 每12小时检查一次


if __name__ == '__main__':
    receiverList = [
        {'address': 'albert.wang@bit-beyond.com', 'phone': '13530155870', 'name': 'wang'},
        # {'address': 'kent.jiang@bit-beyond.com', 'phone': '18033444002', 'name': 'Kent.Jang'}
    ]  # 接收邮件人列表
    loop = asyncio.get_event_loop()
    loop.run_until_complete(check(receiverList))
    loop.run_forever()


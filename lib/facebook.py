#!/usr/bin/env python
# -*- coding:utf-8 -*-

import logging
import redis
import traceback

import tokenlib

PLATFORM = "facebook"  # 平台名
LEVEL = "D"  # 更新间隔，可以在tokenserver查看更多
MAIN_NUM = -1  # 主帐号个数,取帐号文件前main_num个,默认(-1)全部帐号为主账号
BACK_NUM = -1  # 备用帐号个数,取帐号文件后back_num个,默认(-1)不使用备用帐号
MODE = "oss"  # 存储方式,oss或redis(默认)
THREAD_NUM = 1  # 采集token的并发数


class FacebookTokenDictCrawler(tokenlib.BaseTokenCrawler):
    def collect_token(self, *args):
        # pass
        try:
            pool = redis.ConnectionPool(host=args[0], port=5380, db=2, password=args[1])
            r = redis.Redis(connection_pool=pool)
            token_str = r.hgetall("FaceBook_Token_Account_Dict")
            tokens = []
            for i,j in token_str.items():
                dict_ = {}
                dict_[i] = j
                tokens.append(dict_)
            print('FaceBook_Token_Account_dict: /n{}'.format(tokens))
            return tokens
        except:
            logging.error(traceback.format_exc())


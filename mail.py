#!/usr/bin/env python
# -*- coding:utf-8 -*-

import logging
import requests

mail_to = {
    "20002": "nanqi.zhang@bit-beyond.com",
    "20003": "paul.chen@bit-beyond.com",
    "20008": "kevin.yang@bit-beyond.com",
    "20012": "albert.wang@bit-beyond.com",
}

mail_info = u"平台{platform}生成token出错,成功数:{success},失败数:{fail},成功率:{percent}%"

url = "http://api.sendcloud.net/apiv2/mail/send"

API_USER = 'service_bit_beyond'
API_KEY = '7n4u77GuY92c8diL'


def send_email(uid, platform, success, fail, percent):
    info = mail_info.format(platform=platform, success=success, fail=fail, percent=percent)
    params = {
        "apiUser": API_USER,  # 使用api_user和api_key进行验证
        "apiKey": API_KEY,
        "to": ";".join([mail_to[uid], mail_to["20012"]]),  # 收件人地址, 用正确邮件地址替代, 多个地址用';'分隔
        "from": "service@service.bit-beyond.com",  # 发信人, 用正确邮件地址替代
        "fromName": u"TOKEN-SERVER监控",
        "subject": u"token-server错误率 警告邮件",
        "html": info
    }

    try:
        requests.post(url, data=params)
    except:
        logging.error(info)

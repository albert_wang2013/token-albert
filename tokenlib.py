#!/usr/bin/env python
# -*- coding:utf-8 -*-

import csv
import json
import logging
import oss2
import redis

from hashlib import md5

import mail
from fetcher import fetcher

# UID和自己的tornado-api访问端口一致
UID = "20012"

# token-server部署在阿里云的服务器上时设为True，否则设为False
ALI_DOMAIN = False

REDIS_CHANNEL = "{}_redis".format(UID)
OSS_CHANNEL = "{}_oss".format(UID)

_pool = redis.ConnectionPool(host="120.25.255.56", port=8000, db=0, password="7ef615c6c32f427d713144f67e2ef14d248c3ccd4bfd6ddf33a4b56878bbcb2f8c72ae185427d853e08de0aa4a8aa7b3a")
_redis = redis.Redis(connection_pool=_pool)

_auth = oss2.Auth('7JT8SvX8zSsudTIb', 'XawzZPcAXLdapOx5Rn1nptjD7r3Z5U')
if ALI_DOMAIN:
    _bucket = oss2.Bucket(_auth, 'http://oss-cn-shenzhen-internal.aliyuncs.com', 'tornado-api-tokens')
else:
    _bucket = oss2.Bucket(_auth, 'http://oss-cn-shenzhen.aliyuncs.com', 'tornado-api-tokens')


class BaseTokenCrawler(object):
    def __init__(self, platform, level, main_num, back_num=-1, mode="redis", thread_num=5):
        self.platform = platform  # 平台名称,xueqiu
        self.level = level  # H:1小时,D:1天,W:1周,M:1月
        self.main_num = main_num  # 主帐号个数,取帐号文件前main_num个,默认(-1)全部帐号为主账号
        self.back_num = back_num  # 备用帐号个数,取帐号文件后back_num个,默认(-1)不使用备用帐号
        self.token_key = "{uid}_token_{platform}".format(uid=UID, platform=platform)  # tokens在redis中的键,20004_token_xueqiu
        self.file_path = "res/data_{}.csv".format(platform)  # 帐号文件的路径,res/data_xueqiu.csv
        self.mode = mode  # 存储方式，oss或redis(默认)
        self.thread_num = thread_num  # 采集token的并发数

    def collect_token(self, *args):
        """
        **需要在子类中重写该函数**
        **csv文件中帐号信息的保存顺序需要和collect_token函数参数顺序保持一致**
        :param args: 帐号信息
        :return: token
        """
        raise NotImplementedError

    def get_params_list(self):
        """
        如果希望动态获取参数列表，可以重写该方法，默认从/res/data_<platform>.csv中读取数据
        :return:
        """
        with open(self.file_path, "rb") as f:
            reader = csv.reader(f)
            params_list = list(reader)
        return params_list

    def collect_token_list(self, is_main):
        """
        从帐号文件读取帐号信息，采集tokens
        :param is_main: 是否为主账号
        :return: tokens列表
        """
        params_list = self.get_params_list()

        if is_main:  # 采集主账号
            if self.main_num < 0:
                account_list = params_list
            else:
                account_list = params_list[:self.main_num]
        else:  # 采集备用帐号
            account_list = params_list[-self.back_num:]

        token_list = fetcher(self.collect_token, account_list, self.thread_num)

        total = len(account_list)
        success = len(token_list)
        fail = total - success
        percent = float(success) / total * 100

        if percent < 80:
            mail.send_email(UID, self.platform, success, fail, percent)

        return token_list

    def store_token(self, tokens):
        """
        将采集好的token序列化并保存到redis中
        :param tokens: tokens列表
        :return:
        """
        token_str = json.dumps(tokens, separators=(",", ":"))

        try:
            if self.mode == "redis":
                _redis.set(self.token_key, token_str)
            elif self.mode == "oss":
                _bucket.put_object(self.token_key, token_str)

            md5_val = md5(token_str).hexdigest()
            name = "{}_md5_token".format(UID)
            key = "{}:{}".format(self.mode, self.token_key)
            _redis.hset(name, key, md5_val)
        except Exception as e:
            logging.error(e)

    def process(self, is_main):
        """
        采集tokens列表,将tokens保存到redis
        :param is_main: 是否为主账号
        :return:
        """
        tokens = self.collect_token_list(is_main)

        if tokens:
            logging.info("%s get tokens, length: %s", self.platform, len(tokens))
            self.store_token(tokens)

#!/usr/bin/env python
# -*- coding:utf-8 -*-
import datetime
import importlib
import inspect
import logging
import os
import pkgutil
import redis
import time

from Queue import PriorityQueue
from threading import Thread

import tokenlib
import lib


HOST = "120.25.255.56"
PORT = 8000
DB = 0
PASSWORD = "7ef615c6c32f427d713144f67e2ef14d248c3ccd4bfd6ddf33a4b56878bbcb2f8c72ae185427d853e08de0aa4a8aa7b3a"

REDIS_POOL = redis.ConnectionPool(host=HOST, port=PORT, db=DB, password=PASSWORD)
r = redis.Redis(connection_pool=REDIS_POOL)

logging.basicConfig(level=logging.INFO,
                    format='[%(levelname)s %(asctime)s %(filename)s :%(lineno)d]  %(message)s',
                    datefmt='%Y%m%d %H:%M:%S',
                    filename='myapp.log',
                    filemode='a')

LEVEL_MAP = {
    "A": 10,  # 仅供测试用
    "B": 30,  # 仅供测试用
    "X": 60,
    "C": 60 * 5,
    "HH": 60 * 30,
    "H": 60 * 60,
    "HD": 60 * 60 * 12,
    "D": 60 * 60 * 24,
    "W": 60 * 60 * 24 * 7,
    "M": 60 * 60 * 24 * 31,
    "Z": float("inf")
}

THREAD_NUM = 5
DEFAULT_INTERVAL = "D"
BACK_INTERVAL = "X"


def init_task(queue):
    """
    初始化任务队列
    :param queue:
    :return:
    """
    pypath = os.path.dirname(lib.__file__)
    for module_obj in pkgutil.iter_modules([pypath]):
        module_path = "lib.%s" % module_obj[1]
        module = importlib.import_module(module_path)

        cls_members = inspect.getmembers(module, inspect.isclass)
        for cls in cls_members:
            if issubclass(cls[1], tokenlib.BaseTokenCrawler):
                platform = getattr(module, "PLATFORM")
                level = getattr(module, "LEVEL", DEFAULT_INTERVAL)
                main_num = getattr(module, "MAIN_NUM", -1)
                back_num = getattr(module, "BACK_NUM", -1)
                mode = getattr(module, "MODE", "redis")
                thread_num = getattr(module, "THREAD_NUM", 5)

                obj = cls[1](platform, level, main_num, back_num, mode, thread_num)
                cur_stamp = time.time()
                queue.put((cur_stamp, obj, True))
                logging.info("Adding platform:%s, level=%s, main_num=%s, back_num=%s, mode=%s, thread_num=%s", platform, level, main_num, back_num, mode, thread_num)


class Worker(Thread):
    def __init__(self, queue):
        Thread.__init__(self)
        self.daemon = True
        self.queue = queue

    def run(self):
        while True:
            timestamp, task, is_main = self.queue.get()
            curstamp = time.time()
            if curstamp < timestamp:  # 没到更新的时间，把任务放回队列，睡眠一定时间
                self.queue.put((timestamp, task, is_main))
                time.sleep(timestamp - curstamp)
                continue
            task.process(is_main)
            # print "%s updating: %s, %s, %s, %s" % (self.name, task.platform, timestamp, is_main, task.back_num)
            if is_main and task.level == "Z":  # 只更新一次
                continue
            elif is_main and task.back_num > 0:  # 此次更新主账号，下次更新备用帐号
                newstamp = time.time() + LEVEL_MAP[task.level]
                is_main = False
            elif is_main and task.back_num <= 0:  # 此次更新主账号，下次更新主账号，无需备用帐号
                newstamp = time.time() + LEVEL_MAP[task.level]
            else:  # 此次更新备用账号，下次更新主账号
                newstamp = time.time() + LEVEL_MAP[BACK_INTERVAL]
                is_main = True
            self.queue.put((newstamp, task, is_main))


def main():
    queue = PriorityQueue()
    init_task(queue)
    threads = []
    for _ in xrange(THREAD_NUM):
        t = Worker(queue)
        threads.append(t)

    for t in threads:
        t.start()

    for t in threads:
        t.join()

if __name__ == "__main__":
    main()
    logging.info('schedule_main ending!!!! time_sleep 1day')
    """程序定时循环"""
    while True:
        start_ = datetime.datetime.now().replace(microsecond=0)
        if '09:00:00' in str(start_):
            main()
            logging.info('schedule_main ending!!!! time_sleep 1day')
        time.sleep(3600)


